#!/usr/bin/python3
# Uses Bluez for Linux
#
# sudo apt-get install bluez python-bluez
#
# Taken from: https://people.csail.mit.edu/albert/bluez-intro/x232.html
# Taken from: https://people.csail.mit.edu/albert/bluez-intro/c212.html


import bluetooth
import subprocess

port = 1


def receive_messages():
    server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

    server_sock.bind(("", port))
    server_sock.listen(1)

    client_sock, address = server_sock.accept()
    print("Accepted connection from " + str(address))

    data = client_sock.recv(1024)
    print("received [%s]" % data)

    client_sock.close()
    server_sock.close()


def pair(target_bluetooth_mac_address):
    print('sudo bluetoothctl')
    result = subprocess.Popen("sudo bluetoothctl", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result)

    print('power on')
    result = subprocess.Popen("power on", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result)

    print('agent on')
    result = subprocess.Popen("agent on", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout)

    print('discoverable on')
    result = subprocess.Popen("discoverable on", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout)

    print('pairable on')
    result = subprocess.Popen("pairable on", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout)

    print('scan on')
    result = subprocess.Popen("scan on", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout)

    print('pair ')
    result = subprocess.Popen("pair {}".format(target_bluetooth_mac_address), shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    print(result.stdout)

    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    sock.connect((target_bluetooth_mac_address, port))
    return sock


def send_message_to(sock):
    port = 3
    # kill any "bluetooth-agent" process that is already running
    sock.recv(1024)
    sock.send("hello!!")
    sock.close()


def look_up_nearby_bluetooth_devices():
    nearby_devices = bluetooth.discover_devices()
    for bdaddr in nearby_devices:
        print(str(bluetooth.lookup_name(bdaddr)) + " [" + str(bdaddr) + "]")
        sock = pair(bdaddr)
        send_message_to(sock)


look_up_nearby_bluetooth_devices()
