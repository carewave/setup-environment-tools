import time

import serial
import logging.handlers

# Log settings
LOG_NAME = 'volme_device_side'
LOG_PATH = '/var/log/'

logger = logging.getLogger(LOG_NAME)
logger.setLevel(logging.INFO)
# Console log
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_formatter = logging.Formatter('%(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)
# File log
is_log_file = False

try:
    file_handler = logging.handlers.RotatingFileHandler(LOG_PATH + LOG_NAME + '.log', maxBytes=10000, backupCount=5)
    is_log_file = True
except Exception as ex:
    try:
        file_handler = logging.handlers.RotatingFileHandler(LOG_NAME + '.log', maxBytes=10000, backupCount=5)
        is_log_file = True
    except Exception as ex1:
        logger.warn(ex1)

if is_log_file:
    file_handler.setLevel(logging.INFO)
    file_handler.setLevel(logging.DEBUG)
    fileFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(fileFormatter)
    logger.addHandler(file_handler)

# Enable Serial Communication
port = serial.Serial("/dev/ttyS0", baudrate=9600, timeout=1)

# Transmitting AT Commands to the Modem
# '\r\n' indicates the Enter key

while True:
    logger.info('\nEntering the command...\n')
    port.write(str('AT' + '\r\n').encode())
    time.sleep(1)
    rcv = port.readline().decode('ascii')
    time.sleep(1)
    port.flush()
    logger.info('\nAnswer from the module: {}'.format(rcv))
    if rcv is 'OK':
        break
