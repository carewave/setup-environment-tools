########################################################################################################################

                                                      Volme®

########################################################################################################################

This script is intended to work closely with ultrasonic sensors and RaspberryPi.
The logic that's implemented in 'device_side_app.py' triggers an alert if measurement from one
of ultrasonic sensors changes more than a specified threshold.

########################################################################################################################
                                                         Usage
########################################################################################################################

- invocation command: sudo nohup python jsn_sr04_2_0_sensors.py port_trigger:port_echo ... port_trigger:port_echo
host_ip:host_ip username:username password:password &
To get more info by while running the script add '--debug' arg to the base command:
sudo nohup python jsn_sr04_2_0_sensors.py port_trigger:port_echo ... port_trigger:port_echo
host_ip:host_ip --debug &

'mock_device_side_app.py' is intended to mock data that's normally collected from ultrasonic sensors on the
RaspberryPi side.
By running this script you can test new solutions to the device-side script without actual connection to the device.

- invocation command: python mock_device_side_app.py port_trigger:port_echo ... port_trigger:port_echo host_ip:host_ip
