import RPi.GPIO as GPIO
import sys
import traceback
import datetime
import json
import time
import requests
import logging.handlers

CALIBRATION_ITERATIONS_COUNTER = 500
DEFAULT_SENSOR_SETUP_TIME = 0.04
CALIBRATION_SENSOR_SETUP_TIME = 0.04
SENSORS_SETUP_TIME = CALIBRATION_SENSOR_SETUP_TIME
FAULT_COEFFICIENT = 0.1
IS_CALIBRATION = True
# time out for ultrasound wave to go back and forth. This value shouldn't be very small because, sometimes, a sensor
# can wait for a response for a quite long time, due to particularities of a controlled space. For example: in a room a
# wave can bound from from walls or any other obstacles and thus measured distance can reach up to 10 meters (consider,
# that by the data sheet the max measuring distance for the sensor is up to 6 meters).
MAX_DISTANCE_TIME_OUT = 0.08
ALLOWED_FAKE_ALERTS_AMOUNT = 4
LOWER_TOLERANCE_VALUE = 0.9
HIGHER_TOLERANCE_VALUE = 1.1
# use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)
# set GPIO Pins
args = {}
devices_on_quarantine = []
device_to_distance_mapping = {}
max_distance_device_mapping = {}
min_distance_device_mapping = {}
all_measured_values_device_mapping = {}
session = requests.Session()
mock_distance_data = []

# Log settings
LOG_NAME = 'volme_device_side'
LOG_PATH = '/var/log/'

logger = logging.getLogger(LOG_NAME)
logger.setLevel(logging.INFO)
# Console log
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_formatter = logging.Formatter('%(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)
# File log
is_log_file = False

try:
    file_handler = logging.handlers.RotatingFileHandler(LOG_PATH + LOG_NAME + '.log', maxBytes=10000, backupCount=5)
    is_log_file = True
except Exception as ex:
    try:
        file_handler = logging.handlers.RotatingFileHandler(LOG_NAME + '.log', maxBytes=10000, backupCount=5)
        is_log_file = True
    except Exception as ex1:
        logger.warn(ex1)

if is_log_file:
    file_handler.setLevel(logging.INFO)
    file_handler.setLevel(logging.DEBUG)
    fileFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(fileFormatter)
    logger.addHandler(file_handler)

SERVER_HOST = None
USER_NAME = None
PASSWORD = None
PORT = '8088'
POST_EVENT_URL = 'event/volume-change-event/'
DEBUG = False

devices_consequent_alerts_count = {'device1': 0, 'device2': 0, 'device3': 0, 'device4': 0}


def convert_datetime_java_millis(st):
    # Provided a python datetime object convert it into java millis
    return int(time.mktime(st.timetuple()) * 1e3 + st.microsecond / 1e3)


def generate_event(_device_number, _current_distance, _previous_distance, _min_distance, _max_distance):
    known_values = str(all_measured_values_device_mapping[_device_number]['values'])
    _max_distance = str(max_distance_device_mapping[_device_number]['distance'])
    _min_distance = str(min_distance_device_mapping[_device_number]['distance'])
    alert = {'eventType': 'volume_change', 'deviceNumber': _device_number[len(_device_number) - 1],
             'location': ('max_value: %s, min_value: %s || cur_dist: %s, prev_dist: %s, known_values: %s' % (
                 _max_distance, _min_distance, _current_distance, _previous_distance, known_values)),
             'date': str(convert_datetime_java_millis(datetime.datetime.now()))}
    return json.dumps(alert)


def append_data(distance):
    file = open('measurements.txt', 'a+')
    file.write("%s\r\n" % distance)


def parse_args():
    global SERVER_HOST
    global USER_NAME
    global PASSWORD
    global DEBUG
    # TODO: Utilize 'argparse' module to work with arguments
    # TODO: Current implementation allows strict args order only: GPIO pins first all the rest - after.
    for index, arg in enumerate(sys.argv[1:], start=0):

        device_index = index + 1
        arg_key = 'device{}'.format(index + 1)
        if arg.split(':')[0] == 'host_ip':
            SERVER_HOST = arg.split(':')[1]
        elif arg.split(':')[0] == 'username':
            USER_NAME = arg.split(':')[1]
        elif arg.split(':')[0] == 'password':
            PASSWORD = arg.split(':')[1]
        elif arg == '--debug':
            DEBUG = True
        else:
            key, val = arg.split(':')[0], arg.split(':')[1]
            args[arg_key] = {'pin_trigger': key, 'pin_echo': val}
            device_to_distance_mapping['device{}'.format(device_index)] = {'distance': None}
            max_distance_device_mapping['device{}'.format(device_index)] = {'distance': None}
            min_distance_device_mapping['device{}'.format(device_index)] = {'distance': None}
            all_measured_values_device_mapping['device{}'.format(device_index)] = {'values': []}


def post_with_logs(obj, url):
    headers = {'Content-Type': 'application/json'}
    logger.info('\nThe request object is: {}'.format(obj))
    try:
        return session.post(url, obj, headers=headers, auth=(USER_NAME,
                                                             PASSWORD))
    except ConnectionError:
        logger.info('\nConnection error, retrying in 2 seconds...\n')
        time.sleep(2)
        return session.post(url, obj, headers=headers, auth=(USER_NAME,
                                                             PASSWORD))


def is_known_value(known_values, _current_distance):
    match = False
    for i in known_values:
        if match:
            return True
        if i * LOWER_TOLERANCE_VALUE <= _current_distance <= i * HIGHER_TOLERANCE_VALUE:
            match = True
        else:
            continue
    return False


def post_if_alert(_min_distance, _current_distance, _max_distance, _device_number, _previous_distance):
    if is_known_value(list(all_measured_values_device_mapping[_device_number]['values']), _current_distance):
        if DEBUG:
            logger.info('The value {} is already known!'.format(_current_distance))
        is_not_fake_alert(_device_number, False, current_distance)
        return

    if _current_distance < _min_distance * LOWER_TOLERANCE_VALUE \
            or _current_distance > _max_distance * HIGHER_TOLERANCE_VALUE \
            or _current_distance < _max_distance * LOWER_TOLERANCE_VALUE:

        if is_not_fake_alert(_device_number, True, current_distance):
            logger.info('Alert distance from device  %s: %.1f cm : %.1f' % (_device_number, current_distance,
                                                                            previous_distance))
            response = post_with_logs(
                generate_event(_device_number, _current_distance, _previous_distance, _min_distance, _max_distance),
                'http://{}:{}/{}'.format(SERVER_HOST, PORT, POST_EVENT_URL))
            print(response)
            logger.info(response)
            # Resetting fake alerts value to 0, after an alert actually happened for a particular device.
            devices_consequent_alerts_count[_device_number] = 0
    else:
        is_not_fake_alert(_device_number, False, current_distance)


def is_not_fake_alert(_device_number, sensor_registered_alert, _distance):
    global devices_consequent_alerts_count
    alerts_count = devices_consequent_alerts_count[_device_number]
    # checking if an alert for a particular device happened more than some amount of consequent times.
    if alerts_count <= ALLOWED_FAKE_ALERTS_AMOUNT:
        if sensor_registered_alert:
            devices_consequent_alerts_count[_device_number] = alerts_count + 1
        else:
            if devices_consequent_alerts_count[_device_number] == ALLOWED_FAKE_ALERTS_AMOUNT - 1:
                _all_measured_values = all_measured_values_device_mapping[_device_number]['values']
                if not _all_measured_values.__contains__(_distance):
                    _all_measured_values.append(_distance)
                    all_measured_values_device_mapping[_device_number]['values'] = _all_measured_values
            devices_consequent_alerts_count[_device_number] = 0
    else:
        return True

    return False


def run(pin_trigger, pin_echo, sensor_number, _previous_distance):
    global SENSORS_SETUP_TIME

    if not IS_CALIBRATION:
        SENSORS_SETUP_TIME = DEFAULT_SENSOR_SETUP_TIME
    # We will be using the BCM GPIO numbering
    GPIO.setmode(GPIO.BCM)
    # Set TRIGGER to OUTPUT mode
    GPIO.setup(pin_trigger, GPIO.OUT)
    # Set ECHO to INPUT mode
    GPIO.setup(pin_echo, GPIO.IN)

    # Set TRIGGER to LOW
    GPIO.output(pin_trigger, False)

    # Let the sensor settle for a while
    time.sleep(SENSORS_SETUP_TIME)

    # Send 10 microsecond pulse to TRIGGER
    GPIO.output(pin_trigger, True)  # set TRIGGER to HIGH
    time.sleep(0.00001)  # wait 10 microseconds
    GPIO.output(pin_trigger, False)  # set TRIGGER back to LOW

    # Create variable start and give it current time
    start_time = time.time()
    time_out_time = time.time()
    stop_time = time.time()

    # Refresh start value until the ECHO goes HIGH = until the wave is send
    while GPIO.input(pin_echo) == 0:
        time_out = time.time() - time_out_time
        start_time = time.time()
        # measure time out here. If time out is more than SENSORS_RESPONSE_TIME_OUT a fake value will bre returned
        # from run() function
        if time_out > MAX_DISTANCE_TIME_OUT:
            if DEBUG:
                logger.info(
                    '\nThe time out for sending request for sensor {0:} is {1:.2f}'.format(sensor_number, time_out))
            # returning None distance value to keep the script working and to avoid a python process hang because
            # of endless waiting for a response.
            return None
    # Assign the actual time to stop variable until the ECHO goes back from HIGH to LOW
    while GPIO.input(pin_echo) == 1:
        time_out = time.time() - time_out_time
        stop_time = time.time()
        if time_out > MAX_DISTANCE_TIME_OUT:
            if DEBUG:
                logger.info(
                    '\nThe time out for sending request for sensor {0:} is {1:.2f}'.format(sensor_number, time_out))
            return None
    # Calculate the time it took the wave to travel there and back
    measured_time = stop_time - start_time
    # Calculate the travel distance by multiplying the measured time by speed of sound
    rounded_distance = None
    distance = (measured_time * 34300) / 2  # cm/s in 20 degrees Celsius
    if distance is not None:
        rounded_distance = round(distance)
    # Divide the distance by 2 to get the actual distance from sensor to obstacle
    logger.info('Max and Min values: {}:{}'.format(max_distance_device_mapping[device]['distance'],
                                                   min_distance_device_mapping[device]['distance']))
    logger.info('Known distances for sensor %s: ' % device)
    logger.info('%s, ' % (str(all_measured_values_device_mapping[device]['values'])))

    if _previous_distance is None:
        logger.info('Distance from sensor {0:}: {1:.2f} cm : Previous: {2:}'.format(sensor_number, rounded_distance,
                                                                                    _previous_distance))
    else:
        logger.info('Distance from sensor {0:}: {1:.2f} cm : Previous: {2:.2f}'.format(sensor_number, rounded_distance,
                                                                                       _previous_distance))

    # Reset GPIO settings
    GPIO.cleanup()
    time.sleep(0.00002)  # wait 20 microseconds. This is done because of the sensor's inertia
    return rounded_distance


try:
    parse_args()
    IS_CALIBRATION = True
    start_calibration_time = time.time()
    end_calibration_time = time.time()
    while True:
        if CALIBRATION_ITERATIONS_COUNTER == 0:
            IS_CALIBRATION = False
        if DEBUG:
            logger.info('-----------------------------------------------------------')
        if len(devices_on_quarantine) >= 1:
            if DEBUG:
                logger.info('Retrying measuring for sensors:  {}'.format(devices_on_quarantine[0]))
                devices_on_quarantine.clear()
        for device in args:
            if devices_on_quarantine.__contains__(device):
                if DEBUG:
                    logger.info('Device {} is excluded from the loop due a malfunction'.format(device))
                continue
            max_distance = max_distance_device_mapping[device]['distance']
            min_distance = min_distance_device_mapping[device]['distance']
            previous_distance = device_to_distance_mapping[device]['distance']

            current_distance = run(int(args[device]['pin_trigger']),
                                   int(args[device]['pin_echo']),
                                   device, previous_distance)

            if current_distance is None:
                devices_on_quarantine.append(device)
                previous_distance = None
                continue
            # The first iteration distance value is set to None, in 'device_to_distance_mapping' dictionary,
            # to provide our system with an 'initial value to start with'.
            # Starting from the second iteration we can compare two distances and make a decision if it was changed.
            all_measured_values_for_device = all_measured_values_device_mapping[device]['values']
            if IS_CALIBRATION:
                if not all_measured_values_for_device.__contains__(current_distance) and current_distance is not None:
                    all_measured_values_for_device.append(current_distance)
            if previous_distance is None:
                device_to_distance_mapping[device]['distance'] = current_distance
                max_distance_device_mapping[device]['distance'] = current_distance
                min_distance_device_mapping[device]['distance'] = current_distance

            # here trying to find the closest object in the secured area.
            elif IS_CALIBRATION and current_distance < min_distance:
                min_distance_device_mapping[device]['distance'] = current_distance
            elif IS_CALIBRATION and current_distance > max_distance:
                max_distance_device_mapping[device]['distance'] = current_distance
            elif not IS_CALIBRATION:
                post_if_alert(min_distance, current_distance, max_distance, device, previous_distance)
            device_to_distance_mapping[device]['distance'] = current_distance

            if CALIBRATION_ITERATIONS_COUNTER == 1:
                try:
                    maximum_measured = max(all_measured_values_for_device)
                    minimum_measured = min(all_measured_values_for_device)
                    all_measured_values_for_device.remove(maximum_measured)
                    all_measured_values_for_device.remove(minimum_measured)
                    all_measured_values_device_mapping[device]['values'] = all_measured_values_for_device
                except ValueError as e:
                    if DEBUG:
                        logger.info('The value {} has already been deleted, so the deletion attempt caused a '
                                    'ValueException. Proceeding with the application execution normally.'.format(e))
        time.sleep(SENSORS_SETUP_TIME)
        CALIBRATION_ITERATIONS_COUNTER -= 1
        if CALIBRATION_ITERATIONS_COUNTER == 0:
            end_calibration_time = time.time()
            total_calibration_time = end_calibration_time - start_calibration_time
            logger.info('\n-----------------------------------------------------------')
            logger.info('-----------------------------------------------------------\n')
            logger.info('         Calibration finished with time: %.1f seconds        ' % total_calibration_time)
            logger.info('\n-----------------------------------------------------------')
            logger.info('-----------------------------------------------------------\n')


except KeyboardInterrupt:
    logger.info('The process stopped by the user.\n Cleaning GPIO pins...')

except Exception as exception:
    logger.error(exception)
    traceback.print_exc()
finally:
    GPIO.cleanup()
