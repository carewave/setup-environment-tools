import sys
import traceback
import datetime
import json
import time
import requests
import logging.handlers

CALIBRATION_ITERATIONS_COUNTER = 2
FAULT_COEFFICIENT = 0.1
IS_CALIBRATION = True
# time out for ultrasound wave to go back and forth. This value shouldn't be very small because, sometimes, a sensor
# can wait for a response for a quite long time, due to particularities of a controlled space. For example: in a room a
# wave can bound from from walls or any other obstacles and thus measured distance can reach up to 10 meters (consider,
# that by the data sheet the max measuring distance for the sensor is up to 6 meters).
MAX_DISTANCE_TIME_OUT = 0.08
ALLOWED_FAKE_ALERTS_AMOUNT = 0
LOWER_TOLERANCE_VALUE = 0.9
HIGHER_TOLERANCE_VALUE = 1.1
# use Raspberry Pi board pin numbers
devices = {}
args = {}
devices_on_quarantine = []
device_to_distance_mapping = {}
max_distance_device_mapping = {}
min_distance_device_mapping = {}
all_measured_values_device_mapping = {}
session = requests.Session()
mock_distance_data = []

# Log settings
LOG_NAME = 'volme_device_side'
LOG_PATH = '/var/log/'

logger = logging.getLogger(LOG_NAME)
logger.setLevel(logging.INFO)
# Console log
console_handler = logging.StreamHandler()
console_handler.setLevel(logging.INFO)
console_formatter = logging.Formatter('%(message)s')
console_handler.setFormatter(console_formatter)
logger.addHandler(console_handler)
# File log
is_log_file = False

try:
    file_handler = logging.handlers.RotatingFileHandler(LOG_PATH + LOG_NAME + '.log', maxBytes=10000, backupCount=5)
    is_log_file = True
except Exception as ex:
    try:
        file_handler = logging.handlers.RotatingFileHandler(LOG_NAME + '.log', maxBytes=10000, backupCount=5)
        is_log_file = True
    except Exception as ex1:
        logger.warn(ex1)

if is_log_file:
    file_handler.setLevel(logging.INFO)
    file_handler.setLevel(logging.DEBUG)
    fileFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    file_handler.setFormatter(fileFormatter)
    logger.addHandler(file_handler)

SERVER_HOST = None
USER_NAME = None
PASSWORD = None
PORT = '4000'
POST_EVENT_URL = 'events/event/volume-change-event/'
DEBUG = False

# TODO: think of a better mechanism for providing `device_id` and `device_password` (not hardcode. For example,
#  we can read those from a text file, which is going to be created during device manufacturing/tuning).
#  This is more required closer to production stage in order not to change scripts source code each time.

# Credentials to obtain OAuth2 token.
DEVICE_ID = '22223'
DEVICE_PASSWORD = 'nea7v~/8CB\,es4w'
DEVICE_REGISTRATION_URL = None
OAUTH2_TOKEN_URL = None
JWT_OAUTH2_JWT_TOKEN = None

devices_consequent_alerts_count = {'device1': 0, 'device2': 0, 'device3': 0, 'device4': 0}


def register_device():
    global DEVICE_REGISTRATION_URL

    if DEVICE_REGISTRATION_URL is None:
        DEVICE_REGISTRATION_URL = DEVICE_REGISTRATION_URL = 'http://{}:{}/oauth/register/device-registration'.format(
            SERVER_HOST, PORT)

    request_body = {'deviceId': DEVICE_ID, 'password': DEVICE_PASSWORD}
    headers = {'Content-type': 'application/json'}
    response = requests.post(DEVICE_REGISTRATION_URL, data=json.dumps(request_body), headers=headers, verify=False,
                             allow_redirects=False)
    if response.status_code == 201:
        logger.info('The device {} has been successfully registered'.format(DEVICE_ID))
    elif response.status_code == 500:
        logger.info('Device {} has already been registered!'.format(DEVICE_ID))
    else:
        logger.error('Server error occurred during device registration. \n {}'.format(response))


def obtain_oauth2_jwt_token():
    global OAUTH2_TOKEN_URL
    if OAUTH2_TOKEN_URL is None:
        OAUTH2_TOKEN_URL = 'http://{}:{}/oauth/oauth/token'.format(SERVER_HOST, PORT)
    request_body = {'grant_type': 'client_credentials'}
    access_token_response = requests.post(OAUTH2_TOKEN_URL, data=request_body, verify=False, allow_redirects=False,
                                          auth=(DEVICE_ID, DEVICE_PASSWORD))
    tokens = json.loads(access_token_response.text)
    return tokens['access_token']


def convert_datetime_java_millis(st):
    # Provided a python datetime object convert it into java millis
    return int(time.mktime(st.timetuple()) * 1e3 + st.microsecond / 1e3)


def generate_event(device_number):
    _device_id = int(DEVICE_ID)
    alert = {'event_type': 'volume_change', 'device_id': int(_device_id),
             'location': 'ololo_{}'.format(device_number), 'date': str(convert_datetime_java_millis(datetime.datetime
                                                                                                    .now()))}
    return json.dumps(alert)


def populate_mock_distance_data():
    mock_distance_data.append(100)
    mock_distance_data.append(100)
    for i in range(0, 3):
        mock_distance_data.append(120)
    # setting up a higher distance value for device number 4 to simulate more than 20% distance difference
    mock_distance_data.append(130)
    for i in range(0, 3):
        mock_distance_data.append(100)
    # mock_distance_data.append(None)
    mock_distance_data.append(110)
    for i in range(0, 3):
        mock_distance_data.append(100)
    mock_distance_data.append(130)
    for i in range(0, 3):
        mock_distance_data.append(100)
    mock_distance_data.append(130)
    for i in range(0, 2):
        mock_distance_data.append(150)
    mock_distance_data.append(100)
    mock_distance_data.append(90)


def append_data(distance):
    file = open("measurements.txt", "a+")
    file.write("%.1f\r\n" % distance)


def parse_args():
    global SERVER_HOST
    global USER_NAME
    global PASSWORD
    global DEBUG
    # TODO: Utilize 'argparse' module to work with arguments
    # TODO: Current implementation allows strict args order only: GPIO pins first all the rest - after.
    for index, arg in enumerate(sys.argv[1:], start=0):

        device_index = index + 1
        arg_key = 'device{}'.format(index + 1)
        if arg.split(':')[0] == 'host_ip':
            SERVER_HOST = arg.split(':')[1]
        elif arg.split(':')[0] == 'username':
            USER_NAME = arg.split(':')[1]
        elif arg.split(':')[0] == 'password':
            PASSWORD = arg.split(':')[1]
        elif arg == '--debug':
            DEBUG = True
        else:
            key, val = arg.split(':')[0], arg.split(':')[1]
            args[arg_key] = {'pin_trigger': key, 'pin_echo': val}
            device_to_distance_mapping['device{}'.format(device_index)] = {'distance': 100}
            max_distance_device_mapping['device{}'.format(device_index)] = {'distance': None}
            min_distance_device_mapping['device{}'.format(device_index)] = {'distance': None}

            all_measured_values_device_mapping['device{}'.format(device_index)] = {'values': []}


def post_with_logs(obj, url):
    global JWT_OAUTH2_JWT_TOKEN

    if JWT_OAUTH2_JWT_TOKEN is None:
        JWT_OAUTH2_JWT_TOKEN = obtain_oauth2_jwt_token()

    headers = {'Authorization': 'Bearer ' + JWT_OAUTH2_JWT_TOKEN,
               'Content-Type': 'application/json'}
    logger.info('\nThe request object is: {}'.format(obj))
    try:
        response = requests.post(url, obj, headers=headers)
        # it's also possible, that the server will return 504 Zull request routing error. This can be ignored for now.
        # In fact, when the client receives 504 the requested action is actually performed. This should be solved on
        # server side.
        if response.status_code == 401:
            JWT_OAUTH2_JWT_TOKEN = obtain_oauth2_jwt_token()
            post_with_logs(obj, url)

    except ConnectionError:
        logger.info('\nConnection error, retrying in 2 seconds...\n')
        time.sleep(2)
        return session.post(url, obj, headers=headers, auth=(USER_NAME,
                                                             PASSWORD))


def post_if_alert(_min_distance, _current_distance, _max_distance, _device_number):
    max_d = max_distance_device_mapping[_device_number]['distance']
    min_d = min_distance_device_mapping[_device_number]['distance']
    if is_known_value(list(all_measured_values_device_mapping[_device_number]['values']), _current_distance):
        if DEBUG:
            logger.info('The value {} is already known!'.format(_current_distance))
        is_not_fake_alert(_device_number, False, current_distance)
        return

    if _current_distance < min_d * LOWER_TOLERANCE_VALUE \
            or _current_distance > max_d * HIGHER_TOLERANCE_VALUE \
            or _current_distance < max_d * LOWER_TOLERANCE_VALUE:

        if is_not_fake_alert(_device_number, True, current_distance):
            logger.info('Alert distance from device  %s: %.1f cm : %.1f' % (_device_number, current_distance,
                                                                            previous_distance))
            response = post_with_logs(
                generate_event(_device_number),
                'http://{}:{}/{}'.format(SERVER_HOST, PORT, POST_EVENT_URL))
            print(response)
            logger.info(response)
            # Resetting fake alerts value to 0, after an alert actually happened for a particular device.
            devices_consequent_alerts_count[_device_number] = 0


def is_not_fake_alert(_device_number, sensor_registered_alert, _distance):
    global devices_consequent_alerts_count
    alerts_count = devices_consequent_alerts_count[_device_number]
    # checking if an alert for a particular device happened more than some amount of consequent times.
    if alerts_count <= ALLOWED_FAKE_ALERTS_AMOUNT:
        if sensor_registered_alert:
            devices_consequent_alerts_count[_device_number] = alerts_count + 1
        else:
            devices_consequent_alerts_count[_device_number] = 0
    else:
        return True

    return False


def run():
    if len(mock_distance_data) is 0:
        sys.exit('Test data array has ended!')
    return mock_distance_data.pop()


def is_known_value(known_values, _current_distance):
    match = False
    for i in known_values:
        if match:
            return True
        if i * LOWER_TOLERANCE_VALUE <= _current_distance <= i * HIGHER_TOLERANCE_VALUE:
            match = True
        else:
            continue
    return False


try:
    parse_args()
    register_device()
    populate_mock_distance_data()
    is_calibration = True

    while True:
        if CALIBRATION_ITERATIONS_COUNTER == 0:
            IS_CALIBRATION = False
        if DEBUG:
            logger.info('-----------------------------------------------------------')
        if len(devices_on_quarantine) >= 1:
            if DEBUG:
                logger.info('Retrying measuring for sensors:  {}'.format(devices_on_quarantine[0]))
                devices_on_quarantine.clear()
        for device in args:
            if devices_on_quarantine.__contains__(device):
                if DEBUG:
                    logger.info('Device {} is excluded from the loop due a malfunction'.format(device))
                continue
            max_distance = max_distance_device_mapping[device]['distance']
            min_distance = min_distance_device_mapping[device]['distance']

            previous_distance = device_to_distance_mapping[device]['distance']

            current_distance = run()
            logger.info('Known distances for sensor %s: ' % device)
            all_measured_values_device_mapping[device]['values'].sort()
            logger.info('%s, ' % (str(all_measured_values_device_mapping[device]['values'])))

            if current_distance is None:
                devices_on_quarantine.append(device)
                previous_distance = None
                continue
            # The first iteration distance value is set to None, in 'device_to_distance_mapping' dictionary,
            # to provide our system with an 'initial value to start with'.
            # Starting from the second iteration we can compare two distances and make a decision if it was changed.
            all_measured_values_for_device = all_measured_values_device_mapping[device]['values']
            if IS_CALIBRATION:
                if not all_measured_values_for_device.__contains__(current_distance) and current_distance is not None:
                    all_measured_values_for_device.append(current_distance)
            # Adding this if to set max and min distances only ones(this condition will match only once per script
            # lifecycle. This variables are crucial for an alert detection.
            # To check, how exactly min and max values involve an alert detection check `post_if_alert` function.
            if previous_distance is not None and CALIBRATION_ITERATIONS_COUNTER == 2:
                previous_distance = current_distance
                max_distance_device_mapping[device]['distance'] = current_distance
                min_distance_device_mapping[device]['distance'] = current_distance

            # here trying to find the closest object in the secured area.
            elif IS_CALIBRATION and current_distance < min_distance:
                min_distance_device_mapping[device]['distance'] = current_distance
            elif IS_CALIBRATION and current_distance > max_distance:
                max_distance_device_mapping[device]['distance'] = current_distance
            else:
                post_if_alert(min_distance, current_distance, max_distance, device)
            previous_distance = current_distance

            if CALIBRATION_ITERATIONS_COUNTER == 1:
                try:
                    maximum_measured = max(all_measured_values_for_device)
                    minimum_measured = min(all_measured_values_for_device)
                    all_measured_values_for_device.remove(maximum_measured)
                    all_measured_values_for_device.remove(minimum_measured)
                    all_measured_values_device_mapping[device]['values'] = all_measured_values_for_device
                except ValueError as e:
                    if DEBUG:
                        logger.info('The value {} has already been deleted, so the deletion attempt caused a '
                                    'ValueException. Proceeding with the application execution normally.'.format(e))

        CALIBRATION_ITERATIONS_COUNTER -= 1
except Exception:
    traceback.print_exc()
