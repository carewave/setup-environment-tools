import RPi.GPIO as GPIO
import sys
import traceback
import datetime
import json
import time
import requests
import logging.handlers

# use Raspberry Pi board pin numbers
GPIO.setmode(GPIO.BCM)
# set GPIO Pins
args = {}
device_to_distance_mapping = []
session = requests.Session()
mock_distance_data = []

# Log settings
LOG_NAME = "volme_device_side"
LOG_PATH = "/var/log/"

logger = logging.getLogger(LOG_NAME)
logger.setLevel(logging.INFO)
# Console log
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)
consoleFormatter = logging.Formatter('%(message)s')
ch.setFormatter(consoleFormatter)
logger.addHandler(ch)
# File log
isLogFile = False

try:
    fh = logging.handlers.RotatingFileHandler(LOG_PATH + LOG_NAME + '.log', maxBytes=10000, backupCount=5)
    isLogFile = True
except Exception as ex:
    try:
        fh = logging.handlers.RotatingFileHandler(LOG_NAME + '.log', maxBytes=10000, backupCount=5)
        isLogFile = True
    except Exception as ex1:
        logger.warn(ex1)

if isLogFile:
    fh.setLevel(logging.INFO)
    fileFormatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
    fh.setFormatter(fileFormatter)
    logger.addHandler(fh)

SERVER_HOST = None
USER_NAME = None
PASSWORD = None
PORT = '8088'
POST_EVENT_URL = 'event/volume-change-event/'

devices_consequent_alerts_count = {1: 0, 2: 0, 3: 0, 4: 0}


def convert_datetime_java_millis(st):
    # Provided a python datetime object convert it into java millis
    return int(time.mktime(st.timetuple()) * 1e3 + st.microsecond / 1e3)


def generate_event(device_number):
    alert = {'eventType': 'volume_change', 'deviceNumber': device_number,
             'location': 'ololo_{}'.format(device_number), 'date': str(
              convert_datetime_java_millis(datetime.datetime.now()))}
    return json.dumps(alert)


def append_data(distance):
    file = open('measurements.txt', 'a+')
    file.write("%.1f\r\n" % distance)


def parse_args():
    global SERVER_HOST
    global USER_NAME
    global PASSWORD
    # TODO: Utilize 'argparse' module to work with arguments
    for index, arg in enumerate(sys.argv[1:], start=0):

        device_index = index + 1
        arg_key = 'device{}'.format(index + 1)
        if arg.split(':')[0] == 'host_ip':
            SERVER_HOST = arg.split(':')[1]
        elif arg.split(':')[0] == 'username':
            USER_NAME = arg.split(':')[1]
        elif arg.split(':')[0] == 'password':
            PASSWORD = arg.split(':')[1]
        else:
            key, val = arg.split(':')[0], arg.split(':')[1]
            args[arg_key] = {'pin_trigger': key, 'pin_echo': val}
            device_to_distance_mapping.append({'device{}'.format(device_index): None})


def post_with_logs(obj, url, headers):
    logger.info('\nThe request object is: {}'.format(obj))
    try:
        return session.post(url, obj, headers=headers, auth=(USER_NAME,
                                                             PASSWORD))
    except ConnectionError:
        logger.info('\nConnection error, retrying in 2 seconds...\n')
        time.sleep(2)
        return session.post(url, obj, headers=headers, auth=(USER_NAME,
                                                             PASSWORD))


def post_if_alert(previous_distance_, current_distance_, _device_number):
    diff = current_distance_ - previous_distance_
    headers = {'Content-Type': 'application/json'}
    if diff > previous_distance_ * 0.8:
        if is_not_fake_alert(_device_number, True):
            response = post_with_logs(
                generate_event(_device_number),
                'http://{}:{}/{}'.format(SERVER_HOST, PORT, POST_EVENT_URL),
                headers)
            print(response)
            logger.info(response)
            devices_consequent_alerts_count[_device_number] = 0
    else:
        is_not_fake_alert(device_number, False)


def is_not_fake_alert(device_number_, sensor_registered_alert):
    global devices_consequent_alerts_count
    alerts_count = devices_consequent_alerts_count[device_number_]
    # checking if an alert for a particular device happened more than 2 consequent times.
    if alerts_count <= 20:
        if sensor_registered_alert:
            devices_consequent_alerts_count[device_number_] = alerts_count + 1
            logger.info('\n Alerts count: {}'.format(devices_consequent_alerts_count[device_number]))
        else:
            devices_consequent_alerts_count[device_number_] = 0
            logger.info('\n Alerts count: {}'.format(devices_consequent_alerts_count[device_number]))
    else:
        return True

    return


def run(pin_trigger, pin_echo, sensor_number):
    time.sleep(0.00001)
    # set GPIO input and output channels
    GPIO.setup(pin_trigger, GPIO.OUT)
    GPIO.setup(pin_echo, GPIO.IN)
    # set Trigger to HIGH
    GPIO.output(pin_trigger, True)
    # set Trigger after 0.01ms to LOW
    time.sleep(0.00001)
    GPIO.output(pin_trigger, False)

    start_time = time.time()
    stop_time = time.time()
    time_out_time = time.time()
    # save start time
    while GPIO.input(pin_echo) == 0:
        time_out = time.time() - time_out_time
        start_time = time.time()
        if time_out > 1.0:
            return 0.1927
        else:
            logger.debug('\nThe time out is %s'.format(time_out))
    # save time of arrival
    while GPIO.input(pin_echo) == 1:
        time_out = time.time() - time_out_time
        stop_time = time.time()
        if time_out > 1.0:
            return 0.1927
        else:
            logger.debug('\nThe time out is %s'.format(time_out))
    # time difference between start and arrival
    time_elapsed = stop_time - start_time
    # multiply with the sonic speed (34300 cm/s)
    # and divide by 2, because there and back
    distance = (time_elapsed * 34300) / 2
    logger.info('Distance from sensor %d: %.1f cm' % (sensor_number, distance))
    append_data(distance)
    return distance


try:
    parse_args()
    while True:
        device_number = 1
        for device in args:

            current_distance = run(int(args[device]['pin_trigger']),
                                   int(args[device]['pin_echo']),
                                   device_number)
            device_literal = 'device{}'.format(device_number)
            device_from_position = device_to_distance_mapping[device_number - 1]
            previous_distance = device_from_position[device_literal]

            if previous_distance is None:
                device_from_position[device_literal] = current_distance
            else:
                post_if_alert(previous_distance, current_distance,
                              device_number)
            device_number += 1
        time.sleep(0.5)
except Exception as exception:
    logger.error(exception)
    traceback.print_exc()