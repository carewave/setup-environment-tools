#!/usr/bin/python3
import bluetooth
import subprocess

server_socket = bluetooth.BluetoothSocket(bluetooth.RFCOMM)

port = 1
server_socket.bind(("", port))
server_socket.listen(1)

client_socket, address = server_socket.accept()
print("Accepted connection from ", address)


def pair(target_bluetooth_mac_address):
    result = subprocess.run(['sudo', 'bluetoothctl'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['power', 'on'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['agent', 'on'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['discoverable', 'on'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['pairable', 'on'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['scan', 'on'], stdout=subprocess.PIPE)
    print(result.stdout)

    result = subprocess.run(['pair', target_bluetooth_mac_address.encode('utf-8')], stdout=subprocess.PIPE)
    print(result.stdout)

    sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
    sock.connect((target_bluetooth_mac_address, port))


def look_up_nearby_bluetooth_devices():
    print("Inside look_up_nearby_devices")
    nearby_devices = bluetooth.discover_devices()
    for bluetooth_device_address in nearby_devices:
        print(str(bluetooth.lookup_name(bluetooth_device_address)) + " [" + str(bluetooth_device_address) + "]")
        pair(bluetooth_device_address)


#look_up_nearby_bluetooth_devices()
while True:

    data = client_socket.recv(1024)
    print("Received: %s" % data)

    if data.decode('utf-8') == "q":
        print("Quit")
        break

client_socket.close()
server_socket.close()
